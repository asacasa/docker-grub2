FROM debian:buster

RUN apt update; \
	apt install -y \
		build-essential \
		automake

RUN apt install -y gettext-base git
RUN apt install -y autopoint
RUN apt install -y autoconf
RUN apt install -y python libtool
RUN apt install -y bison flex
RUN apt install -y pkg-config
RUN apt install -y libdevmapper-dev
RUN apt install -y autotools-dev

ADD grub /src

RUN mkdir /install /build; cd /src; ./bootstrap

CMD cd /src; \
	./configure --with-platform=efi --target=x86_64; \
	make -j `cat /proc/cpuinfo | grep process | wc -l`; \
	make install DESTDIR=/export; \
	/export/usr/local/bin/grub-mkstandalone -O x86_64-efi -o /export/mygrub.efi
